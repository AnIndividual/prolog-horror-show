% Specifies the dynamic fact used to end the program and loads the code stored in the listed files.
:- dynamic end_loop/1.
:- ['data_base.txt'],
   ['characters.txt'],
   ['writing.txt'],
   ['menu_actions.txt'].
   
% default values
end_loop(true).

% ****************************************
%          Program Entry
% ****************************************

% main run method. Initializes the game and starts the menu loop.
run :- retract(end_loop(true)),
	   assert(end_loop(false)),
	   get_style(Style),
	   get_player_number(NumPlayers),
	   initialize(Style,NumPlayers),
	   get_players(Players),
	   initialize_hand_sizes(Players), !,
	   loop, !.
	   
% Used to prompt the user to specify the hand size of all players.
initialize_hand_sizes([]).
initialize_hand_sizes([Player|T]) :- get_cards_in_hand(Player,Num), initialize_player_hand_size(Player,Num), initialize_hand_sizes(T).

% ****************************************
%          Main Loop
% ****************************************

% The termination case for the game's menu loop.
loop :- end_loop(true).

% Prints the main menu then asks the user to specify an action and performs it.
loop :- current_menu(main),
		nl,
		write('---------------------'), nl,
		write('|     Main Menu     |'), nl,
		write('---------------------'), nl,
		write_current_solutions,
		get_main_action(Action),
		call(Action),
		loop.

% Prints the query menu the asks the user to specify an action and performs it.
loop :- current_menu(query),
		nl,
		write('---------------------'), nl,
		write('|    Query Menu     |'), nl,
		write('---------------------'), nl,
		get_query_action(Action),
		call(Action),
		loop.
		
% ****************************************
%          Main Menu Actions
% ****************************************

% Prints all the information that can be pulled from the data stored about the current game.
print_current_state :- nl, print_facts, nl.

% Initializes a query and goes to the query menu.
go_to_query_menu :- nl,
					write('What is the character being asked for?'), nl,
					get_character(C), nl,
					write('What is the weapon being asked for?'), nl,
					get_weapon(W), nl,
					write('What is the room being asked for?'), nl,
					get_room(R), nl,
					set_current_character(C),
					set_current_weapon(W),
					set_current_room(R),
					set_current_menu(query).
					
% Prompts the user for information on a card that was revealed and logs the reveal.
enter_revealed_card :- nl,
					   get_player(Player), nl,
					   get_card_type(Type), nl,
					   get_card(Type, Card), nl,
					   player_revealed(Player,Card).

% Displays the help information.					   
show_help :- nl,
			 write('Specifying your cards:'), nl,
			 write('   Use the "Set player has cards" option to initialize your hand. This'), nl,
			 write('   option is also used to log cards revealed by other players.'), nl, nl,
			 write('Logging query results:'), nl,
			 write('   Use the "Entery query responses" option to log the results of queries'), nl,
			 write('   made to players. It is important to log the results of queries you make'), nl,
			 write('   as well as those made by other players. Logging that a player revealed or'), nl,
			 write('   passed is important. If a player shows a card to another player the program'), nl,
			 write('   my be able identify what that card was. When players reveal a card to you'), nl,
			 write('   you do not need to log the query reveal. Just specify that they have the card.'), nl, nl,
			 write('Current facts legend:'), nl,
			 write('   X = has card or solution'), nl,
			 write('   * = does not have card'), nl,
			 write('   ? = might have card or might be solution'), nl.
					   
% Releases all the stored data and ends the current game.
exit_game :- clear_stored_data, retract(end_loop(false)), assert(end_loop(true)).

% ****************************************
%          Query Menu Actions
% ****************************************

% Logs a player response to the current query.
log_query_response :- nl,
					  get_player(Player), nl,
					  get_query_response(Response), nl,
					  current_character(C),
					  current_weapon(W),
					  current_room(R),
					  log_query_answer(C,W,R,Player,Response).

% Returns to the main menu.
return_to_main_menu :- set_current_menu(main).

% ****************************************
%          Helpers
% ****************************************

% A generic method for asking the user to specify a card based on the type of card.
get_card(character, Card) :- write('Which Character?'), nl, get_character(Card).
get_card(weapon, Card) :- write('Which Weapon?'), nl, get_weapon(Card).
get_card(room, Card) :- write('Which Room?'), nl, get_room(Card).

% Used to write the currently known solutions at the top of the main menu.
write_current_solutions :- write('Currently known solutions:'), nl, get_cards(Cards), print_solutions(Cards), !, nl.

% A recursive method to help print the known solutions.
print_solutions([]).
print_solutions([Card|T]) :- solution_state(Card,solution), !, write_card(Card), nl, print_solutions(T).
print_solutions([Card|T]) :- not(solution_state(Card,solution)), print_solutions(T).