# Prolog Horror Show

A command line tool to help you play Clue written in Prolog. Why would you do this? Why would anyone do this? It's a bad idea and it doesn't even work right, but here it is.

## How do you run it?

First you're going to need a Prolog environment like [SWI-Prolog](http://www.swi-prolog.org/). Unfortunately, the web version is too simple to run this so you'll need to install it.

Next you'll need to [load the program](http://www.swi-prolog.org/). This will initialize the "logic" and put it in a state where it's ready to run. To actually start it, declare "run" by typing "run." and pressing enter. The program executes as a command line program using prolog's execution order to print a menu and ask for inputs.

## How do I use it?

First, the program will ask you for some initial state information. The board style just controls the naming of the weapons and rooms. You'll also need to give it the number of players (including you) and the number of cards in each player's hand. Note that you'll have to type a "." after each value before you press enter for Prolog to recognize it as an input. Once your past initialization you'll find yourself at the main menu.

~~~
---------------------
|     Main Menu     |
---------------------
Currently known solutions:

What would you like to do?
p = Print currently known facts
q = Enter query responses
s = Set player has card
h = Show help information
e = Exit
|: 
~~~

If you type 'p' you'll see someting like this.

~~~
              | Me  P1  P2  P3 | Sln
------------------------------------
Ms. Scarlet   |                |  ? 
Prof. Plumb   |                |  ? 
Col. Mustard  |                |  ? 
Mr. Green     |                |  ? 
Mrs. White    |                |  ? 
Mrs. Peacock  |                |  ? 
------------------------------------
Knife         |                |  ? 
Candlestick   |                |  ? 
Pistol        |                |  ? 
Rope          |                |  ? 
Bat           |                |  ? 
Axe           |                |  ? 
------------------------------------
Kitchen       |                |  ? 
Patio         |                |  ? 
Spa           |                |  ? 
Theater       |                |  ? 
Living Room   |                |  ? 
Observatory   |                |  ? 
Hall          |                |  ? 
Guest House   |                |  ? 
Dining Room   |                |  ?
~~~

As you provide the program with facts it will fill this in with information you can use to help guide your choices. It will highlight solutions once it's certain of them. You can get some helpful information about how to do this using the "h" option reproduced here with spelling errors intact.

~~~
Specifying your cards:
   Use the "Set player has cards" option to initialize your hand. This
   option is also used to log cards revealed by other players.

Logging query results:
   Use the "Entery query responses" option to log the results of queries
   made to players. It is important to log the results of queries you make
   as well as those made by other players. Logging that a player revealed or
   passed is important. If a player shows a card to another player the program
   my be able identify what that card was. When players reveal a card to you
   you do not need to log the query reveal. Just specify that they have the card.

Current facts legend:
   X = has card or solution
   * = does not have card
   ? = might have card or might be solution
~~~

Start by using the "s" command to input your own cards. When another player makes a query use the "q" option to specify the query and what each player's response was. When you make a query use the "q" option to enter negative responses and the "s" command if a player reveals a card to you.

As you play the game you'll eventually notice the fact sheet printed by the "p" option eventually doesn't line up with reality. This is a know issue and will not be fixed.